"call pathogen#infect()
"call pathogen#helptags()
set nocompatible 
set autoread
set nu sts=4 sw=4 ts=4 ai fen expandtab hlsearch wmh=0 t_Co=256 laststatus=2 
set cinoptions=:0,l1,t0,g0
set nocsverb
" Pathogen load
filetype on

set backspace=2 " make backspace work like most other apps

set pastetoggle=<F5>

"filetype on
filetype indent on
filetype plugin on
set statusline=%<%f\%h%m%r%=%-20.(line=%l\ \ col=%c%V\ \ totlin=%L%)\ \ \%h%m%r%=%-40(bytval=0x%B,%n%Y%)\%P
"set tags=./tags,tags,~/.tags.cocoa,~/projects/current/tags
syntax on
"set t_Co=256
"colorscheme koehler
"colorscheme calmar256-dark
"colorscheme 256_asu1dark
"colorscheme zellner
colorscheme desert256
"colorscheme summerfruit256
"colorscheme vividchalk
autocmd FileType htmlcheetah runtime! indent/html.vim
let g:html_indent_tags = '1'
au Filetype html,xml,xsl,mako,jinja source ~/.vim/plugin/closetag.vim
au BufRead,BufNewFile *.ly set filetype=yacc
au BufRead,BufNewFile *.x set filetype=lex
au BufRead,BufNewFile *.i set filetype=swig
au BufRead,BufNewFile *.hsc set filetype=haskell
au BufRead,BufNewFile *.hs set filetype=haskell
set omnifunc=haskellcomplete#CompleteHaskell
au BufRead,BufNewFile *.thrift set filetype=thrift
au BufRead,BufNewFile *.mako set filetype=mako
au BufRead,BufNewFile *.md set filetype=mkd
au BufRead,BufNewFile *.go set filetype=go
au BufRead,BufNewFile *.c  set foldmethod=syntax
au BufRead,BufNewFile *.m  set filetype=objc foldmethod=syntax
au BufRead,BufNewFile *.inc  set filetype=php foldmethod=syntax sts=2 ts=2 sw=2
au BufRead,BufNewFile *.module  set filetype=php foldmethod=syntax sts=2 ts=2 sw=2
au BufRead,BufNewFile *.php  set filetype=php foldmethod=syntax sts=2 ts=2 sw=2
au BufRead,BufNewFile *.rb  set filetype=ruby foldmethod=syntax sts=2 ts=2 sw=2
au BufRead,BufNewFile *.caml set filetype=yaml foldmethod=syntax sts=2 ts=2 sw=2
au BufRead,BufNewFile *.html set filetype=html foldmethod=syntax sts=2 ts=2 sw=2
" if !exists("autocommands_loaded")
"       let autocommands_loaded = 1
"         autocmd BufRead,BufNewFile,FileReadPost *.py source ~/.vim/python
"     endif

" This beauty remembers where you were the last time you edited the file,
" and returns to the same position.
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
" PYTHON STUFF 
au BufRead,BufNewFile *.py  set ts=4 sw=4 sta et sts=2 ai
autocmd FileType python set omnifunc=pythoncomplete#Complete

au BufRead,BufNewFile *.cgi  set ts=2 ts=2 sw=2
au BufRead,BufNewFile *.apache.conf  set filetype=apache
au BufRead,BufNewFile *.yml  set filetype=yaml foldmethod=indent
au BufRead,BufNewFile *.wsgi  set filetype=python 
au BufRead,BufNewFile TODO set filetype=todo foldmethod=syntax
au BufRead,BufNewFile *.java set filetype=java foldmethod=syntax sts=4 ts=4 sw=4
au BufRead,BufNewFile *.cpp set filetype=cpp foldmethod=syntax sts=4 ts=4 sw=4

"au BufRead,BufNewFile *.m  cscope add /Users/bran/projects/lark/Lark/cocoa.cscope
"au BufRead,BufNewFile *.m  cscope add /Users/bran/projects/lark/Lark/project.cscope

let maplocalleader = ","
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax

"vmap <C-c> y:call system("glipper", getreg("\""))<CR>
"nmap <C-v> :call setreg("\"",system("glipper"))<CR>p
" On ubuntu (running Vim in gnome-terminal)

set clipboard=unnamed

set mouse=a

vmap <C-x> :!pbcopy<CR>  
vmap <C-c> :w !pbcopy<CR><CR> 

set wrap
set linebreak

map <F7> <Esc>:setlocal spell spelllang=en_us
map <S-F7> <Esc>:setlocal nospell

let g:pydiction_location =  '/home/gideon/.vim/after/ftplugin/pydiction/complet-dict' 

nmap <silent> <S-k> :wincmd k<CR>
nmap <silent> <S-j> :wincmd j<CR>
nmap <silent> <S-h> :wincmd h<CR>
nmap <silent> <S-l> :wincmd l<CR>
nmap <silent> <S-Up> :wincmd k<CR>
nmap <silent> <S-Down> :wincmd j<CR>
nmap <silent> <S-Left> :wincmd h<CR>
nmap <silent> <S-Right> :wincmd l<CR>

" disable pymode indentation
let g:pymode_indent = 0

cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))
cnoreabbrev <expr> Q ((getcmdtype() is# ':' && getcmdline() is# 'Q')?('q'):('Q'))
cnoreabbrev <expr> WQ ((getcmdtype() is# ':' && getcmdline() is# 'WQ')?('wq'):('WQ'))
cnoreabbrev <expr> Wq ((getcmdtype() is# ':' && getcmdline() is# 'Wq')?('wq'):('Wq'))
